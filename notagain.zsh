notagain(){
    # why care about errors? ;p
    #tmux select-window -t $1 || $@ 2> /dev/null
    tmux set-option -g display-time 1500
    if tmux select-window -t $1 2> /dev/null;
    then 
	    tmux display-message "Not again! $1 is here!";
    else 
	    tmux display-message "Running $@"; $@ 2> /dev/null;
    fi
}
